#### Run tests   

Required java 1.8 32bit
   
1) update connection data in src/main/resources/test.properties   
2) mvn package   
3) cd test/target  
4) java -jar sqlxd-test-runable.jar -generate -path=<directory>   
5) java -jar sqlxd-test-runable.jar -test db.type=<DB2|ORACLE|SQLxD> v=<1|2> -path=<directory>