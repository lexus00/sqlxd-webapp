package pl.edu.agh.sqlxd.test.db;

import static pl.edu.agh.sqlxd.test.sql.QueryType.COUNT_ROWS;
import static pl.edu.agh.sqlxd.test.sql.QueryType.CREATE_INVOICES_TABLE;
import static pl.edu.agh.sqlxd.test.sql.QueryType.DELETE_ALL_INVOICES;
import static pl.edu.agh.sqlxd.test.sql.QueryType.IS_INVOICES_TABLE_EXISTS;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.apache.log4j.Logger;

import pl.edu.agh.sqlxd.test.sql.QueryResult;
import pl.edu.agh.sqlxd.test.sql.QueryType;

public abstract class SqlDB implements DB {
   private final Logger logger = Logger.getLogger(getClass());

   protected final String url;
   protected final String user;
   protected final String password;
   protected final String jdbcDriverClass;

   private Connection connection;
   
   protected SqlDB(String jdbcDriverClass, String url) {
      this(jdbcDriverClass, url, null, null);
   }
   
   protected SqlDB(String jdbcDriverClass, String url, String user, String password) {
      super();
      this.jdbcDriverClass = jdbcDriverClass;
      this.url = url;
      this.user = user;
      this.password = password;
      logger.info(String.format("url: %s, user=%s, password=%s", url, user, password));
   }


   protected final Connection connection() throws SQLException {
      return connection; 
   }

   @Override
   public void initConnection() throws SQLException {
      classForName(jdbcDriverClass);
      this.connection = DriverManager.getConnection(url, user, password);
      this.connection.setAutoCommit(false);

      if ( !isInvoicesTableExists() ){
         logger.info("create INVOICES(JSON) table");
         createInvoicesTable();
      }
   }

   protected static void classForName(final String clazz) throws SQLException {
      try {
         Class.forName(clazz);
      } catch (ClassNotFoundException e) {
         throw new SQLException(e);
      }
   }

   @Override
   public void closeConnection() throws SQLException {
      if (connection != null) {
         connection.rollback();
         connection.close();
         connection = null;
      }
      logger.info("close connection");
   }

   @Override
   public boolean isInvoicesTableExists() throws SQLException {
      try {
         System.out.println(getQuery(IS_INVOICES_TABLE_EXISTS));
         final Statement stmt = connection.createStatement();
         final ResultSet rs = stmt.executeQuery(getQuery(IS_INVOICES_TABLE_EXISTS));
         final boolean exists = rs.next();
         stmt.close();
         connection.commit();
         return exists;
      } catch (SQLException ex) {
         return false;
      }
   }

   public void createInvoicesTable() throws SQLException {
      final Statement stmt = connection.createStatement();
      final String query = getQuery(CREATE_INVOICES_TABLE);
      logger.debug("Create query: " + query);
      stmt.execute(query);
      stmt.close();
      connection.commit();
   }

   @Override
   public void clearInvoicesTable() throws SQLException {
      final Statement stmt = connection.createStatement();
      stmt.execute(getQuery(DELETE_ALL_INVOICES));
      stmt.close();
      connection.commit();
   }

   @Override
   public Long countInvoicesTableRows() throws SQLException {
      try {
         final Statement stmt = connection().createStatement();
         final ResultSet rs = stmt.executeQuery(getQuery(COUNT_ROWS));
         final Long nrows = rs.next() ? rs.getLong(1) : null;
         stmt.close();
         connection().commit();
         return nrows;
      } catch (SQLException ex) {
         return -1L;
      }
   }
   
   @Override
   public QueryResult query(QueryType queryType) throws SQLException {
//      logger.debug("execute: " + getQuery(queryType).replaceAll("\\s+", " "));
      final PreparedStatement stmt = connection().prepareStatement(getQuery(queryType));
      
      if (QueryType.GET_TOTAL_INCOME_AFTER_SOME_TIME.equals(queryType)) {
         stmt.setLong(1, QueryType.DATE_TOTAL_INCOME_QUERY);
      }
      
      final ResultSet rs = stmt.executeQuery();
      final QueryResult queryResponse = new QueryResult(rs.getMetaData());
      while (rs.next()) {
         queryResponse.addRow(rs);
      }
      stmt.close();
      connection().commit();
      return queryResponse;
   }

   protected abstract QueryProvider getQueryProvider();
   
   protected String getQuery(QueryType type){
      return getQueryProvider().getQuery(type);
   }
   protected void setConnection(Connection connection) {
      this.connection = connection;
   }

}