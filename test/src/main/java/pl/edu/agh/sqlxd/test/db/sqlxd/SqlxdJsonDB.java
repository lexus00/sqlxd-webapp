package pl.edu.agh.sqlxd.test.db.sqlxd;

import java.util.List;

public class SqlxdJsonDB extends SqlxdDB {
   
   public SqlxdJsonDB() {
      super(new SqlxdJsonQueryProvider());
   }

   @Override
   public void saveInvoices(List<String> filepaths) throws Exception {
      for (String filepath : filepaths) {
         getConnection().loadJSON(filepath);
         getConnection().commit();
      }
   }

}
