package pl.edu.agh.sqlxd.test.data.xml;

import java.io.File;
import java.io.IOException;

import javax.xml.bind.SchemaOutputResolver;
import javax.xml.transform.stream.StreamResult;

import javax.xml.transform.Result;

public class SchemaOutputResolverImpl extends SchemaOutputResolver {

   private static final String SCHEMA_PATH_PATTERN = "%s/schema.xsd";
   
   private final String schemaFilePath;
   
   public SchemaOutputResolverImpl(String outputDir){
      this.schemaFilePath = String.format(SCHEMA_PATH_PATTERN, outputDir);
   }
   
   public Result createOutput(String namespaceURI, String suggestedFileName) throws IOException {
      final File file = new File(schemaFilePath);
      final StreamResult result = new StreamResult(file);
      result.setSystemId(file.toURI().toURL().toString());
      return result;
   }

}
