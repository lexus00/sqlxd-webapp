package pl.edu.agh.sqlxd.test.db.db2;

import static pl.edu.agh.sqlxd.test.sql.QueryType.INSERT_INVOICE;

import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

import pl.edu.agh.sqlxd.test.utils.FileUtils;

public class DB2Json extends DB2 {

   public DB2Json() {
      super(new DB2JsonQueryProvider());
   }
   


   @Override
   public void saveInvoices(List<String> files) throws SQLException, IOException {
      final PreparedStatement insertPreparedStmt = connection().prepareStatement(getQuery(INSERT_INVOICE));
      for (String file : files) {
         insertPreparedStmt.setString(1, FileUtils.readFileToString(file));
         insertPreparedStmt.executeUpdate();
      }

      insertPreparedStmt.close();
      connection().commit();
   }

}
