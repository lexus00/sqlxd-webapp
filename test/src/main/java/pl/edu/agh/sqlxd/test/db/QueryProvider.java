package pl.edu.agh.sqlxd.test.db;

import java.util.Map;

import pl.edu.agh.sqlxd.test.sql.QueryType;

public abstract class QueryProvider {
   
   public final String getQuery(QueryType type) {
      String query = getQueries().get(type);
      if (query == null)
         throw new UnsupportedOperationException("Query not supported: " + type);
      return query;
   }
   
   protected abstract Map<QueryType, String> getQueries();
}
