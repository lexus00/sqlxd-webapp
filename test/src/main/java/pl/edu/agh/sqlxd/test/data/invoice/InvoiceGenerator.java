package pl.edu.agh.sqlxd.test.data.invoice;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Random;
import java.util.Set;

public class InvoiceGenerator {
   private final long startYearTime = 40 * 31_104_000_000L;
   private static final char[] CHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".toCharArray();

   private final Random random;
   private final Locale[] locales;
   private final Set<String> invoiceNumbers;
   private final Map<Integer, String> products;
   private final Map<Integer, Buyer> buyers;

   public InvoiceGenerator() {
      this.random = new Random();
      this.locales = Locale.getAvailableLocales();
      this.invoiceNumbers = new HashSet<>();
      this.products = new HashMap<>();
      this.buyers = new HashMap<>();
   }

   public Invoice generateInvoice() {
      final Date date = randomDate();
      final Invoice inv = new Invoice(invoiceNumer(date), date, getOrCreateBuyer());

      int n = randomNumber(2, 100);
      for (int i = 0; i < n; ++i)
         inv.getProducts().add(getOrCreateProduct());

      return inv;
   }

   private Product getOrCreateProduct() {
      int id;
      String code;
      if (products.size() > 50 && random.nextDouble() < 0.2) {
         Map.Entry<Integer, String> entry = getRandomItem(products.entrySet());
         id = entry.getKey();
         code = entry.getValue();
      } else {
         id = randomNumber(1000, 999999999);
         if (products.containsKey(id)) {
            code = products.get(id);
         } else {
            code = String.format("P/%04d/%s/%03d",
                  randomNumber(0, 10000), randomString(4), randomNumber(0, 1000));
            products.put(id, code);
         }

      }

      int amount = randomNumber(1, 1000);
      double value = amount * randomNumber(1, 100) + random.nextDouble();
      return new Product(id, code, amount,
            BigDecimal.valueOf(value).setScale(2, RoundingMode.CEILING));
   }

   private Buyer getOrCreateBuyer() {
      if (buyers.size() > 50 && random.nextDouble() < 0.3)
         return getRandomItem(buyers.values());

      Integer taxId = randomNumber(1000000, 9999999);
      if (buyers.containsKey(taxId))
         return buyers.get(taxId);

      String code = String.format("B/%s/%04d/%s/%02d",
            randomCountry(), randomNumber(0, 10000), randomString(4), randomNumber(0, 100));
      Buyer buyer = new Buyer(taxId, code);

      buyers.put(taxId, buyer);
      return buyer;
   }


   @SuppressWarnings({"deprecation"})
   private String invoiceNumer(Date date) {
      String number = String.format("FV/%02d/%02d/%04d", date.getYear() + 1900,
            date.getMonth() + 1, randomNumber(1, 10000));
      if (invoiceNumbers.contains(number)) {
         number = invoiceNumer(date);
      } else {
         invoiceNumbers.add(number);
      }
      return number;
   }

   private int randomNumber(int from, int to) {
      return from + random.nextInt(to - from);
   }

   private <T> T getRandomItem(Collection<T> collection) {
      List<T> list = new ArrayList<T>(collection);
      return list.get(random.nextInt(list.size()));
   }

   private Date randomDate() {
      return new Date(startYearTime + random.nextInt(720) * 86_400_000L);
   }

   private String randomString(int n) {
      StringBuilder sb = new StringBuilder();
      for (int i = 0; i < n; ++i)
         sb.append(CHARS[random.nextInt(24)]);
      return sb.toString();
   }

   private String randomCountry() {
      return locales[random.nextInt(locales.length)].getCountry();
   }
}
