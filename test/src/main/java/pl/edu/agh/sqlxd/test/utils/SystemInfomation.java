package pl.edu.agh.sqlxd.test.utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.lang.management.ManagementFactory;
import java.util.Date;

import org.apache.log4j.Logger;

import com.sun.management.OperatingSystemMXBean;

public class SystemInfomation extends Thread {
   private static final Logger logger = Logger.getLogger(SystemInfomation.class);
  
   private final OperatingSystemMXBean operatingSystem;
   private final long totalMemorySize;
   private final PrintStream printer;
   private volatile boolean running;
   
   private StringBuilder buffer;
     
   public SystemInfomation() throws FileNotFoundException {
      operatingSystem = (OperatingSystemMXBean) ManagementFactory.getOperatingSystemMXBean();
      totalMemorySize = operatingSystem.getTotalPhysicalMemorySize();
      running = true;
      buffer = new StringBuilder();

      final File file = new File("system-information.txt");
      printer = new PrintStream(new FileOutputStream(file, true));
      logger.info(String.format("output file: %s", file.getAbsolutePath()));
   }

   public void run() {
      createHeader();
      while (running) {
         logInformation();
         safeSleep(250);
      }
   }
   
   public void stopRunning() {
      running = false;

      safeSleep(300);
      printer.println(buffer.toString());
      printer.println();
      printer.close();
   }
   
   public void createHeader() {
      printer.println(new Date());
      printer.println("Free Memory Size [MB]; Used Memory Size [MB]; "
            + "CPU usage for the whole system [%]; CPU usage for the Java Virtual Machine process [%] ");
   }

   public void logInformation() {
      final long freeMemorySize = operatingSystem.getFreePhysicalMemorySize();
      final long usedMemorySize = totalMemorySize - freeMemorySize;
      final double systemCpuLoad = operatingSystem.getSystemCpuLoad();
      final double processCpuLoad = operatingSystem.getProcessCpuLoad();

      buffer
            .append(toMegabyte(freeMemorySize)).append("; ")
            .append(toMegabyte(usedMemorySize)).append("; ")
            .append(toProc(systemCpuLoad)).append("; ")
            .append(toProc(processCpuLoad))
            .append("\n");
   }

   private double toMegabyte(long number) {
      return number / 1048576.0;
   }
   
   private double toProc(double number) {
      return ((long) (number * 100000)) / 1000.0;
   }
   
   private void safeSleep(long millis) {
      try {
         Thread.sleep(millis);
      } catch (InterruptedException e) {
      }
   }
}
