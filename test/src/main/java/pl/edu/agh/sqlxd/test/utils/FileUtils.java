package pl.edu.agh.sqlxd.test.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UncheckedIOException;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

public class FileUtils {

   public static InputStream fileToStream(String file) {
      try {
         return new FileInputStream(file);
      } catch (FileNotFoundException e) {
         throw new UncheckedIOException(e);
      }
   }
   
   public static InputStream resourceAsStream(final String filepath) {
      return FileUtils.class.getClassLoader().getResourceAsStream(filepath);
   }
   
   public static Reader fileToReader(String file) {
      return new InputStreamReader(fileToStream(file), StandardCharsets.UTF_8);
   }
     
   public static Stream<String> readFile(String file) throws IOException {
      final InputStream is = fileToStream(file);
      final BufferedReader br = new BufferedReader(new InputStreamReader(is, StandardCharsets.UTF_8));      
      Stream<String> stream = br.lines();
      return stream;
   }
   
   public static String readFileToString(String file) throws IOException {
      final StringBuilder sb = new StringBuilder();
      FileUtils.readFile(file).forEach(line -> sb.append(line));
      return sb.toString();
   }
   
   public static List<File> getInvoicesList(final String directory) {
      final File[] invoices = new File(directory).listFiles(getInvoiceFileNameFilter());
      return Arrays.asList(invoices);
   }
   private static FilenameFilter getInvoiceFileNameFilter() {
      return new FilenameFilter() {
         public boolean accept(File dir, String name) {
            if (name == null || "".equals(name.trim()))
               return false;
            return name.startsWith("invoice") &&
                  (name.endsWith("xml") || name.endsWith("json"));
         }
      };
   }
}
