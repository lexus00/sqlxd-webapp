package pl.edu.agh.sqlxd.test;

import static java.lang.Math.min;
import static java.util.Arrays.asList;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.stream.Collectors;

import org.apache.log4j.Logger;

import pl.edu.agh.sqlxd.test.db.DB;
import pl.edu.agh.sqlxd.test.sql.QueryResult;
import pl.edu.agh.sqlxd.test.sql.QueryType;
import pl.edu.agh.sqlxd.test.utils.FileUtils;
import pl.edu.agh.sqlxd.test.utils.SystemInfomation;

public class PerformanceTest {
   private static final Logger logger = Logger.getLogger(PerformanceTest.class);
   
   private final DB database;
   private final SimpleDateFormat sdf;  
   private final List<Integer> loadInvoicesSteps;   
   private final List<File> invoices;   
   private final List<QueryType> queries;
   private final SystemInfomation systemInfomation;
      
   public PerformanceTest(final String workdir, final DB database, final String v) throws Exception {
      this.sdf = new SimpleDateFormat("HH:mm:ss.SSS");
      this.invoices = FileUtils.getInvoicesList(workdir);
      this.loadInvoicesSteps = "1".equals(v) 
            ? asList(min(500,invoices.size())) 
            : asList(10, 25, 50, 75, 100, 250, 500);
      this.queries = QueryType.getSelectQueries();
      this.database = database;
      
//      if ((!"1".equals(v)) && invoices.size() < 500) {
//         System.err.println(String.format("Not enough invoice documents in directory %s. "
//               + "Required at least 500", workdir));
//         System.exit(-1);
//      }
      
      database.initConnection();
      database.clearInvoicesTable();

      systemInfomation = new SystemInfomation();
   }
   

   public void performTest() throws Exception {
      Writer generalStatistics = createStatisticWriter("generalStatistics.txt");
//      logTestStart(generalStatistics, database);

//      log(generalStatistics, " loaded invoices number (step): ");
//      loadInvoicesSteps.forEach(number -> log(generalStatistics, number + "; "));
//      log(generalStatistics, "\n query-type; execute time (for each step) [ms]");
//      log(generalStatistics, "\n");
      

      final Map<QueryType, List<Long>> queryExecutionTime = createQueryExecutionTimeMap();
      
      Integer invoicesStartIndex = 0;
      systemInfomation.start();
      for (Integer invoicesEndIndex : loadInvoicesSteps) {
         long loadTime = loadInvoices(database, invoicesStartIndex, invoicesEndIndex);
         queryExecutionTime.get(QueryType.INSERT_INVOICE).add(loadTime);
         invoicesStartIndex = invoicesEndIndex;
         
         logger.debug("execute queries");
         queries.forEach(type -> {
            final long executionTime = executeQuery(database, type);
            queryExecutionTime.get(type).add(executionTime);  
            logger.debug("finish query: " + type);
            intervalBetweenQueries();
         });
      }
      systemInfomation.stopRunning();
      
      String header = "";
      String dataRow = "";
      
      for (Map.Entry<QueryType, List<Long>> entry : queryExecutionTime.entrySet()) {
         for (Long time : entry.getValue()) {
            header += entry.getKey().name().replaceAll("_", " ") + ";";
            dataRow += time + ";";
         }
      }

//      log (generalStatistics, header + "\n");
      log (generalStatistics, dataRow + "\n");
    
      generalStatistics.close();
      logger.info("Test finished");
   }

   
   
   private void intervalBetweenQueries() {
      try {
         Thread.sleep(1000);
      } catch (Exception e) {
         e.printStackTrace();
      }
   }


   private void log(Writer generalStatistics, String value) {
      try {
         generalStatistics.write(value);
      } catch (IOException e) {
         logger.error(e.getMessage());
      }
   }

   
   
   private long loadInvoices(DB db, Integer invoicesStartIndex, Integer invoicesEndIndex) throws Exception {
      long startTimeMills = System.currentTimeMillis();
      List<File> invoicesSubList = invoices.subList(invoicesStartIndex, invoicesEndIndex);
      List<String> invoicesPath = invoicesSubList.stream()
            .map(File::getAbsolutePath)
            .collect(Collectors.toList());
      db.saveInvoices(invoicesPath); 
      long endTimeMills = System.currentTimeMillis();
      return endTimeMills - startTimeMills;
   }



   private long executeQuery(DB db, QueryType queryType) {
      try {
         final long startTimeMills = System.currentTimeMillis();
         final QueryResult queryResult = db.query(queryType);
//         System.out.println(queryResult);
         final long endTimeMills = System.currentTimeMillis();
         return endTimeMills - startTimeMills;
      } catch (Exception e) {
         logger.error(queryType + " " + e.getMessage(), e);
         return Long.MAX_VALUE;
      }
   }
   
   private Writer createStatisticWriter(String filename) throws IOException {
      File file = new File(filename);
      if (!file.exists()) {
         file.createNewFile();
      }
      logger.info(String.format("output file: %s", file.getAbsolutePath()));
      FileWriter generalStatistics = new FileWriter(file, true);      
      return generalStatistics;
   }
   private void logTestStart(Writer writer, DB db) throws IOException {
      writer.write(String.format("\nTest start at %s on database %s\n", current(), db.getDatabaseName()));
   }
   private String current(){
      return sdf.format(new Date());
   } 
   private Map<QueryType, List<Long>> createQueryExecutionTimeMap() {
      Map<QueryType, List<Long>> map = new TreeMap<>();
      map.put(QueryType.INSERT_INVOICE, new ArrayList<>());
      queries.forEach(type -> map.put(type, new ArrayList<>()));
      return map;
   }
}
