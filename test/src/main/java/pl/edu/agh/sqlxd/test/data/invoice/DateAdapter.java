package pl.edu.agh.sqlxd.test.data.invoice;

import java.util.Date;

import javax.xml.bind.annotation.adapters.XmlAdapter;

public class DateAdapter extends XmlAdapter<Long, Date> {

   @Override
   public Long marshal(Date v) throws Exception {
      return v != null ? v.getTime() : null;
   }

   @Override
   public Date unmarshal(Long v) throws Exception {
      return v != null ? new Date(v) : null;
   }

}