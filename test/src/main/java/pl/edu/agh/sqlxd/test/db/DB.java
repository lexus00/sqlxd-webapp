package pl.edu.agh.sqlxd.test.db;

import pl.edu.agh.sqlxd.test.sql.QueryResult;
import pl.edu.agh.sqlxd.test.sql.QueryType;

import java.util.List;

public interface DB {
   String getDatabaseName();

   void initConnection() throws Exception;

   void closeConnection() throws Exception;

   boolean isInvoicesTableExists() throws Exception;

   void clearInvoicesTable() throws Exception;

   Long countInvoicesTableRows() throws Exception;

   QueryResult query(QueryType queryType) throws Exception;

   void saveInvoices(List<String> files) throws Exception;
}
