package pl.edu.agh.sqlxd.test.data.invoice;

import java.math.BigDecimal;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "product")
public class Product {

   @XmlAttribute
   protected int id;

   @XmlElement
   protected String code;

   @XmlElement
   protected int amount;

   @XmlElement
   protected BigDecimal totalCost;

   public Product() {
   }

   public Product(int id, String code, int amount, BigDecimal totalCost) {
      super();
      this.id = id;
      this.code = code;
      this.amount = amount;
      this.totalCost = totalCost;
   }

   public int getId() {
      return id;
   }

   public void setId(int id) {
      this.id = id;
   }

   public String getCode() {
      return code;
   }

   public void setCode(String name) {
      this.code = name;
   }

   public int getAmount() {
      return amount;
   }

   public void setAmount(int amount) {
      this.amount = amount;
   }

   public BigDecimal getTotalCost() {
      return totalCost;
   }

   public void setTotalCost(BigDecimal totalCost) {
      this.totalCost = totalCost;
   }
}
