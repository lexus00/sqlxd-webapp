package pl.edu.agh.sqlxd.test;

import pl.edu.agh.sqlxd.test.db.db2.DB2;
import pl.edu.agh.sqlxd.test.db.db2.DB2Json;
import pl.edu.agh.sqlxd.test.db.mongo.MongoDB;
import pl.edu.agh.sqlxd.test.db.oracle.OracleDB;
import pl.edu.agh.sqlxd.test.db.oracle.OracleJsonDB;
import pl.edu.agh.sqlxd.test.db.sqlxd.SqlxdDB;
import pl.edu.agh.sqlxd.test.db.sqlxd.SqlxdJsonDB;

public class SqlxdMain {

   public static final String path = "C:\\Users\\Blazej\\Downloads\\invoices-json";
   public static final String pathXml = "C:\\Users\\Blazej\\Downloads\\invoices-xml";
   
   public static void main(String[] args) throws Exception {
      final MongoDB db = new MongoDB();

//      final DB2 db = new DB2();
//      final DB2Json db = new DB2Json();

//      final OracleDB db = new OracleDB();
//      final OracleJsonDB db = new OracleJsonDB();

//      final SqlxdJsonDB db = new SqlxdJsonDB();
//      final SqlxdDB db = new SqlxdDB();
      
      db.initConnection();
      db.countInvoicesTableRows();
      
      String v = System.getProperty("testType");
      
//      final PerformanceTest performanceTest = new PerformanceTest(path, db, "2");
      final PerformanceTest performanceTest = new PerformanceTest(path, db, v);
//      final PerformanceTest performanceTest = new PerformanceTest(pathXml, db, "1");
      performanceTest.performTest();
   }


}
