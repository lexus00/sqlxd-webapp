package pl.edu.agh.sqlxd.test.db.db2;

import static pl.edu.agh.sqlxd.test.sql.QueryType.COUNT_ROWS;
import static pl.edu.agh.sqlxd.test.sql.QueryType.CREATE_INVOICES_TABLE;
import static pl.edu.agh.sqlxd.test.sql.QueryType.DELETE_ALL_INVOICES;
import static pl.edu.agh.sqlxd.test.sql.QueryType.GET_INVOICES_NUMBER;
import static pl.edu.agh.sqlxd.test.sql.QueryType.GET_PRODUCTS_DATA;
import static pl.edu.agh.sqlxd.test.sql.QueryType.GET_TOTAL_INCOME_AFTER_SOME_TIME;
import static pl.edu.agh.sqlxd.test.sql.QueryType.GROUP_BY_INVOICE_ID_ORDER_BY_INVOICE_VALUE;
import static pl.edu.agh.sqlxd.test.sql.QueryType.GROUP_BY_PRODUCT_ID;
import static pl.edu.agh.sqlxd.test.sql.QueryType.INSERT_INVOICE;
import static pl.edu.agh.sqlxd.test.sql.QueryType.IS_INVOICES_TABLE_EXISTS;

import java.util.HashMap;
import java.util.Map;

import pl.edu.agh.sqlxd.test.db.QueryProvider;
import pl.edu.agh.sqlxd.test.sql.QueryType;

public class DB2JsonQueryProvider extends QueryProvider {

   private Map<QueryType, String> queries;

   public DB2JsonQueryProvider() {
      initQueries();
   }

   protected Map<QueryType, String> getQueries() {
      return queries;
   }

   private void initQueries() {
      queries = new HashMap<>();

      queries.put(IS_INVOICES_TABLE_EXISTS, "SELECT 1 FROM SYSCAT.TABLES WHERE upper(tabname) = upper('invoices_json')");

      queries.put(CREATE_INVOICES_TABLE,
            "CREATE TABLE invoices_json (" + 
                  "id BIGINT NOT NULL GENERATED ALWAYS AS IDENTITY (START WITH 1 INCREMENT BY 1), " + 
                  " \"doc\" BLOB(16777216) INLINE LENGTH 3000, " + 
                  " \"ROWCREATED\" TIMESTAMP(12) NOT NULL DEFAULT CURRENT TIMESTAMP )");

      queries.put(INSERT_INVOICE, "INSERT INTO invoices_json (\"doc\") VALUES (SYSTOOLS.JSON2BSON(?))");
      queries.put(COUNT_ROWS, "SELECT count(*) as \"rows_number\" FROM invoices_json");
      queries.put(DELETE_ALL_INVOICES, "DELETE FROM invoices_json");

      queries.put(GET_INVOICES_NUMBER,
            "SELECT JSON_VAL(INV.\"doc\", 'number', 's:256') as \"invoice_number\" " + 
            "FROM invoices_json INV");

      queries.put(GET_PRODUCTS_DATA,
            "SELECT" + 
            "   JSON_VAL(INV.\"doc\", 'number', 's:256') as \"invoice_number\"," + 
            "   JSON_VAL(SYSTOOLS.JSON2BSON(PRODUCT.value), 'id', 's:30') as \"id\"," + 
            "   JSON_VAL(SYSTOOLS.JSON2BSON(PRODUCT.value), 'code', 's:256') as \"code\"," + 
            "   JSON_VAL(SYSTOOLS.JSON2BSON(PRODUCT.value), 'amount', 'i') as \"amount\"," + 
            "   JSON_VAL(SYSTOOLS.JSON2BSON(PRODUCT.value), 'totalCost', 'n') as \"totalCost\"" + 
            "FROM invoices_json INV, " + 
            "   TABLE(SYSTOOLS.JSON_TABLE(INV.\"doc\", 'products', 's:32672')) PRODUCT");

      queries.put(GROUP_BY_PRODUCT_ID,
            "SELECT" + 
            "   \"product_id\", \"product_code\", SUM(\"product_totalCost\")" + 
            "FROM " + 
            "   ( SELECT " + 
            "         JSON_VAL(SYSTOOLS.JSON2BSON(PROD.value), 'id', 's:30') as \"product_id\"," + 
            "         JSON_VAL(SYSTOOLS.JSON2BSON(PROD.value), 'code', 's:256') as \"product_code\"," + 
            "         JSON_VAL(SYSTOOLS.JSON2BSON(PROD.value), 'totalCost', 'n') as \"product_totalCost\"" + 
            "      FROM invoices_json INV, " + 
            "         TABLE(SYSTOOLS.JSON_TABLE(INV.\"doc\", 'products', 's:32672')) PROD ) " + 
            "GROUP BY \"product_id\", \"product_code\"");

      queries.put(GROUP_BY_INVOICE_ID_ORDER_BY_INVOICE_VALUE,
            "SELECT" + 
            "   \"invoice_number\", \"buyer_code\", SUM(\"product_totalCost\") as \"sum_totalCost\"" + 
            "FROM " + 
            "   ( SELECT " + 
            "      JSON_VAL(INV.\"doc\", 'number', 's:256') as \"invoice_number\"," + 
            "      JSON_VAL(INV.\"doc\", 'buyer.id', 's:256') as \"buyer_code\"," + 
            "      JSON_VAL(SYSTOOLS.JSON2BSON(PROD.value), 'totalCost', 'n') as \"product_totalCost\"" + 
            "    FROM invoices_json INV, " + 
            "      TABLE(SYSTOOLS.JSON_TABLE(INV.\"doc\", 'products', 's:32672')) PROD )" + 
            "GROUP BY" + 
            "   \"invoice_number\", \"buyer_code\"" + 
            "ORDER BY" + 
            "   \"sum_totalCost\"");

      queries.put(GET_TOTAL_INCOME_AFTER_SOME_TIME,
            "SELECT" + 
            "   COALESCE(SUM(\"product_totalCost\"), 0) as \"sum_totalCost\"" + 
            "FROM " + 
            "   ( SELECT " + 
            "      JSON_VAL(INV.\"doc\", 'date', 'l') as \"date\"," + 
            "      JSON_VAL(SYSTOOLS.JSON2BSON(PROD.value), 'totalCost', 'n') as \"product_totalCost\"" + 
            "    FROM invoices_json INV, " + 
            "      TABLE(SYSTOOLS.JSON_TABLE(INV.\"doc\", 'products', 's:32672')) PROD )" + 
            "WHERE " + 
            "   \"date\" > ?");

   }
}
