package pl.edu.agh.sqlxd.test.db.db2;

import static pl.edu.agh.sqlxd.test.sql.QueryType.INSERT_INVOICE;
import static pl.edu.agh.sqlxd.test.utils.PropertiesUtil.DB2_PASSWORD_KEY;
import static pl.edu.agh.sqlxd.test.utils.PropertiesUtil.DB2_URL_KEY;
import static pl.edu.agh.sqlxd.test.utils.PropertiesUtil.DB2_USER_KEY;
import static pl.edu.agh.sqlxd.test.utils.PropertiesUtil.getProperty;

import java.io.IOException;
import java.io.InputStream;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

import pl.edu.agh.sqlxd.test.db.QueryProvider;
import pl.edu.agh.sqlxd.test.db.SqlDB;
import pl.edu.agh.sqlxd.test.utils.FileUtils;

public class DB2 extends SqlDB {
   private static final String DB2_DRIVER = "com.ibm.db2.jcc.DB2Driver";
   
   private QueryProvider queryProvider;
   
   public DB2() {
      super(DB2_DRIVER, getProperty(DB2_URL_KEY), getProperty(DB2_USER_KEY), getProperty(DB2_PASSWORD_KEY));      
      this.queryProvider = new DB2QueryProvider();
   }

   protected DB2(QueryProvider queryProvide) {
      super(DB2_DRIVER, getProperty(DB2_URL_KEY), getProperty(DB2_USER_KEY), getProperty(DB2_PASSWORD_KEY));      
      this.queryProvider = queryProvide;
   }

   @Override
   public void saveInvoices(List<String> files) throws SQLException, IOException {
      final PreparedStatement insertPreparedStmt = connection().prepareStatement(getQuery(INSERT_INVOICE));
      for (String file : files) {
         InputStream is = FileUtils.fileToStream(file);
         insertPreparedStmt.setBinaryStream(1, is);
         insertPreparedStmt.executeUpdate();
         is.close();
      }

      insertPreparedStmt.close();
      connection().commit();
   }

   @Override
   protected QueryProvider getQueryProvider() {
      return queryProvider;
   }


   @Override
   public String getDatabaseName() {
      return "DB2";
   }
}
