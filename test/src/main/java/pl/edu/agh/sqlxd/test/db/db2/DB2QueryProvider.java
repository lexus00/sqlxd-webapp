package pl.edu.agh.sqlxd.test.db.db2;

import static pl.edu.agh.sqlxd.test.sql.QueryType.*;

import java.util.HashMap;
import java.util.Map;

import pl.edu.agh.sqlxd.test.db.QueryProvider;
import pl.edu.agh.sqlxd.test.sql.QueryType;

public class DB2QueryProvider extends QueryProvider {

   private Map<QueryType, String> queries;

   public DB2QueryProvider() {
      initQueries();
   }

   protected Map<QueryType, String> getQueries() {
      return queries;
   }

   private void initQueries() {
      queries = new HashMap<>();

      queries.put(IS_INVOICES_TABLE_EXISTS, "SELECT 1 FROM SYSCAT.TABLES "
            + "WHERE upper(tabname) = 'INVOICES'");

      queries.put(CREATE_INVOICES_TABLE,
            "CREATE TABLE invoices ("
                  + "id BIGINT NOT NULL GENERATED ALWAYS AS IDENTITY (START WITH 1 INCREMENT BY 1)"
                  + ", document XML)");

      queries.put(INSERT_INVOICE, "INSERT INTO invoices (document) VALUES (?)");
      queries.put(COUNT_ROWS, "SELECT count(*) as \"rows_number\" FROM invoices");
      queries.put(DELETE_ALL_INVOICES, "DELETE FROM invoices");

      queries.put(GET_INVOICES_NUMBER,
            "SELECT XMLQUERY ('$doc/invoice/number/text()' "
                  + "passing document as \"doc\") as \"invoice_number\" FROM invoices");

      queries.put(GET_PRODUCTS_DATA,
            "SELECT X.* FROM XMLTABLE ('db2-fn:xmlcolumn(\"INVOICES.DOCUMENT\")/invoice/product' "
                  + "COLUMNS invoice_number VARCHAR(50) PATH '../number/text()', product_id INTEGER PATH '@id', code VARCHAR(50) "
                  + "PATH 'code', totalCost DECIMAL(12,2) PATH 'totalCost', amount INTEGER PATH 'amount') AS X");

      queries.put(GROUP_BY_PRODUCT_ID,
            "SELECT product_id, code, sum(totalCost) as \"sum_total_cost\" "
                  + "FROM XMLTABLE ('db2-fn:xmlcolumn(\"INVOICES.DOCUMENT\")/invoice/product' COLUMNS product_id INTEGER PATH '@id', "
                  + " code VARCHAR(50) PATH 'code', totalCost DECIMAL(12,2) PATH 'totalCost') "
                  + "AS X GROUP BY product_id, code ORDER BY product_id, code");

      queries.put(GROUP_BY_INVOICE_ID_ORDER_BY_INVOICE_VALUE,
            "SELECT invoice_number, buyer_code, sum(totalCost) as \"sum_total_cost\" "
                  + " FROM XMLTABLE ('db2-fn:xmlcolumn(\"INVOICES.DOCUMENT\")/invoice/product' COLUMNS invoice_number VARCHAR(50) "
                  + " PATH '../number/text()', buyer_code VARCHAR(50) PATH '../buyer/code/text()', totalCost DECIMAL(12,2) PATH 'totalCost') "
                  + " AS X GROUP BY invoice_number, buyer_code ORDER BY \"sum_total_cost\"");

      queries.put(GET_TOTAL_INCOME_AFTER_SOME_TIME,
            "SELECT COALESCE(sum(totalCost), 0) as \"total_income\" "
                  + "FROM XMLTABLE ('db2-fn:xmlcolumn(\"INVOICES.DOCUMENT\")/invoice/product' COLUMNS invoice_number VARCHAR(50) PATH '../number/text()', "
                  + "buyer_code VARCHAR(50) PATH '../buyer/code/text()', sold_date BIGINT PATH '../EpochTime/text()', totalCost DECIMAL(12,2) PATH 'totalCost') AS X where sold_date >= ?");

   }
}
