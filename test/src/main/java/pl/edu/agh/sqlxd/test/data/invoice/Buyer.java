package pl.edu.agh.sqlxd.test.data.invoice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "buyer")
public class Buyer {

   @XmlElement(name="taxId")
   protected int id;

   @XmlElement
   protected String code;

   public Buyer() {
   }

   public Buyer(int id, String code) {
      super();
      this.id = id;
      this.code = code;
   }

   public int getId() {
      return id;
   }

   public void setId(int taxId) {
      this.id = taxId;
   }

   public String getCode() {
      return code;
   }

   public void setCode(String code) {
      this.code = code;
   }
}
