package pl.edu.agh.sqlxd.test.db.mongo;

import com.mongodb.BasicDBObject;
import pl.edu.agh.sqlxd.test.sql.QueryType;

import java.util.List;

import static com.mongodb.BasicDBObject.parse;
import static java.util.Arrays.asList;

public class MongoDbQueryProvider {


   public List<String> getColumnsName(final QueryType queryType) {
      switch (queryType) {
         case COUNT_ROWS:
            return asList("rows_number");
         case GET_PRODUCTS_DATA:
            return asList("number", "id", "code", "totalCost", "amount");

         case GROUP_BY_PRODUCT_ID:
            return asList("totalCost", "product_id", "code");

         case GET_INVOICES_NUMBER:
            return asList("invoice_number");

         case GROUP_BY_INVOICE_ID_ORDER_BY_INVOICE_VALUE:
            return asList("invoice_number", "buyer_code", "totalCost");

         case GET_TOTAL_INCOME_AFTER_SOME_TIME:
            return asList("total_income");

         default:
            throw new IllegalArgumentException("unsupported query type " + queryType);
      }
   }

   public List<BasicDBObject> getAggregatePipeline(final QueryType queryType) {
      switch (queryType) {
         case GET_PRODUCTS_DATA:
            return getProductsDataQuery();

         case GROUP_BY_PRODUCT_ID:
            return getProductByIdQuery();

         case GET_INVOICES_NUMBER:
            return getInvoicesNumberQuery();

         case GROUP_BY_INVOICE_ID_ORDER_BY_INVOICE_VALUE:
            return getGroupByInvoiceIdOrderByInvoiceValue();

         case GET_TOTAL_INCOME_AFTER_SOME_TIME:
            return getTotalIncomeAfterSomeTimeQuery();

         default:
            throw new IllegalArgumentException("unsupported query type " + queryType);
      }
   }

   private List<BasicDBObject> getTotalIncomeAfterSomeTimeQuery() {
      final BasicDBObject match = parse(String.format("{ $match : { date : {$gte : %d}}}", QueryType.DATE_TOTAL_INCOME_QUERY));
      final BasicDBObject group = parse("{ $group : { \"_id\" : false, \"total_income\" : { $sum : { $sum : \"$products" +
            ".totalCost\"}} }}");
      return asList(match, group);
   }

   private List<BasicDBObject> getGroupByInvoiceIdOrderByInvoiceValue() {
      final BasicDBObject project = parse("{ $project : { \"_id\" : false,\"invoice_number\" : \"$number\",\"buyer_code\" : \"$buyer" +
            ".id\",\"totalCost\" : {$sum :" +
            " \"$products.totalCost\"}}}");
      final BasicDBObject sort = parse("{ $sort : {\"totalCost\" : 1} }");
      return asList(project, sort);
   }

   private List<BasicDBObject> getInvoicesNumberQuery() {
      final BasicDBObject project = parse("{ $project : { \"_id\" : false, \"invoice_number\" : \"$number\" }}");
      return asList(project);
   }

   private List<BasicDBObject> getProductByIdQuery() {
      final BasicDBObject unwindProducts = parse("{ $unwind : \"$products\" }");
      final BasicDBObject project1 = parse("{ $project : { \"_id\" : {\"product_id\" : \"$products.id\",\"code\" : " +
            "\"$products.code\"}, \"totalCost\" : \"$products.totalCost\"}}");
      final BasicDBObject group = parse("{ $group:{ \"_id\" : \"$_id\", \"totalCost\" : {$sum : \"$totalCost\" }}}");
      final BasicDBObject project2 = parse("{ $project : { \"_id\" : false, \"product_id\" : \"$_id.product_id\", " +
            "\"code\" : \"$_id.code\", " +
            "\"totalCost\" : true }}");
      return asList(unwindProducts, project1, group, project2);
   }

   private List<BasicDBObject> getProductsDataQuery() {
      final BasicDBObject unwindProducts = parse("{ $unwind : \"$products\" }");
      final BasicDBObject projectValues = parse("{ $project : { \"_id\" : false,\"number\" : true,\"id\" : " +
            "\"$products.id\",\"code\" : \"$products.code\",\"amount\" : \"$products.amount\",\"totalCost\" : \"$products" +
            ".totalCost\"}}");
      return asList(unwindProducts, projectValues);
   }
}
