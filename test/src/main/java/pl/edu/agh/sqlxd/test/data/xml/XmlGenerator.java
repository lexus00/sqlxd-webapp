package pl.edu.agh.sqlxd.test.data.xml;

import org.apache.log4j.Logger;
import pl.edu.agh.sqlxd.test.data.invoice.InvoiceGenerator;
import pl.edu.agh.sqlxd.test.data.invoice.Buyer;
import pl.edu.agh.sqlxd.test.data.invoice.Invoice;
import pl.edu.agh.sqlxd.test.data.invoice.Product;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.SchemaOutputResolver;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public class XmlGenerator {
   private static final Logger logger = Logger.getLogger(XmlGenerator.class);
   private static final String FILE_PATH_PATTERN = "%s" + File.separator + "invoice%04d.xml";

   private int lastInvoiceNumber;
   private final String outputDir;
   private final JAXBContext modelPojoContext;

   private InvoiceGenerator invoiceGenerator;

   public XmlGenerator(final String outputDir) throws JAXBException {
      this.modelPojoContext = JAXBContext.newInstance(Buyer.class, Product.class, Invoice.class);
      this.outputDir = outputDir;
      this.lastInvoiceNumber = 0;
      this.invoiceGenerator = new InvoiceGenerator();
   }


   public void createSchema() throws IOException {
      final SchemaOutputResolver schemaOutputResolver = new SchemaOutputResolverImpl(outputDir);
      modelPojoContext.generateSchema(schemaOutputResolver);
   }

   public void createInvoices(int numberOfDocs) throws JAXBException {
      final Marshaller marshaller = createMarshaller();
      for (int idx = 1; idx <= numberOfDocs; ++idx) {
         final Invoice inv = invoiceGenerator.generateInvoice();
         try (final FileOutputStream fos = new FileOutputStream(nextInvoiceFilepath())) {
            marshaller.marshal(inv, fos);
         } catch (Exception e) {
            logger.error(e.getMessage(), e);
         }
      }

      logger.info(String.format("Created %d Invoices XML documents in '%s' dir", numberOfDocs, outputDir));
   }


   private String nextInvoiceFilepath() {
      String filepath = String.format(FILE_PATH_PATTERN, outputDir, ++lastInvoiceNumber);
      while (new File(filepath).exists()) {
         filepath = String.format(FILE_PATH_PATTERN, outputDir, ++lastInvoiceNumber);
      }
      return filepath;
   }


   private Marshaller createMarshaller() throws JAXBException {
      final Marshaller marshaller = modelPojoContext.createMarshaller();
      marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
      marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
      marshaller.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
      return marshaller;
   }


}
