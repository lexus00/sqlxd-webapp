package pl.edu.agh.sqlxd.test.data.json;

import java.io.File;
import java.io.FileOutputStream;

import org.apache.log4j.Logger;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import pl.edu.agh.sqlxd.test.data.invoice.Invoice;
import pl.edu.agh.sqlxd.test.data.invoice.InvoiceGenerator;
import pl.edu.agh.sqlxd.test.data.xml.XmlGenerator;

public class JsonGenerator {
   private static final Logger logger = Logger.getLogger(XmlGenerator.class);
   private static final String FILE_PATH_PATTERN = "%s" + File.separator + "invoice%04d.json";

   private int lastInvoiceNumber;
   private final String outputDir;

   private InvoiceGenerator invoiceGenerator;
   private ObjectMapper objectMapper;

   public JsonGenerator(final String outputDir) {
      this.outputDir = outputDir;
      this.lastInvoiceNumber = 0;
      this.invoiceGenerator = new InvoiceGenerator();
      this.objectMapper = new ObjectMapper().enable(SerializationFeature.WRAP_ROOT_VALUE);
   }

   public void createInvoices(int numberOfDocs) {
      for (int idx = 1; idx <= numberOfDocs; ++idx) {
         final Invoice inv = invoiceGenerator.generateInvoice();
         try (final FileOutputStream fos = new FileOutputStream(nextInvoiceFilepath())) {
            objectMapper.writeValue(fos, inv);
         } catch (Exception e) {
            logger.error(e.getMessage(), e);
         }
      }

      logger.info(String.format("Created %d Invoices JSON documents in '%s' dir", numberOfDocs, outputDir));
   }

   private String nextInvoiceFilepath() {
      String filepath = String.format(FILE_PATH_PATTERN, outputDir, ++lastInvoiceNumber);
      while (new File(filepath).exists()) {
         filepath = String.format(FILE_PATH_PATTERN, outputDir, ++lastInvoiceNumber);
      }
      return filepath;
   }

}
