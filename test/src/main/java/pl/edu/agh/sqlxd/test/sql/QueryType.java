package pl.edu.agh.sqlxd.test.sql;

import java.util.Arrays;
import java.util.List;

public enum QueryType {
   
   IS_INVOICES_TABLE_EXISTS,
   CREATE_INVOICES_TABLE,
   
   INSERT_INVOICE,
   DELETE_ALL_INVOICES,

   COUNT_ROWS,
   GET_INVOICES_NUMBER,
   GET_PRODUCTS_DATA,    
   GROUP_BY_PRODUCT_ID,
   GROUP_BY_INVOICE_ID_ORDER_BY_INVOICE_VALUE,
   GET_TOTAL_INCOME_AFTER_SOME_TIME;
   
   public static final long DATE_TOTAL_INCOME_QUERY = 1244160000000L; // + 360 * 86_400_000L; // BASE_VALUE + HALF_OF_RANDOM
   
   private static final List<QueryType> SELECT_QUERIES = Arrays.asList(
         COUNT_ROWS, GET_INVOICES_NUMBER, GET_PRODUCTS_DATA,
         GROUP_BY_PRODUCT_ID, GROUP_BY_INVOICE_ID_ORDER_BY_INVOICE_VALUE,
         GET_TOTAL_INCOME_AFTER_SOME_TIME);

   public static List<QueryType> getSelectQueries() {
      return SELECT_QUERIES;
   }
   
}
