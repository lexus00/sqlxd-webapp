package pl.edu.agh.sqlxd.test.db.sqlxd;

import static pl.edu.agh.sqlxd.test.utils.PropertiesUtil.SQLXD_URL_KEY;
import static pl.edu.agh.sqlxd.test.utils.PropertiesUtil.getProperty;

import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.List;

import org.apache.log4j.Logger;
import org.sqlite.SQLiteConnection;

import pl.edu.agh.sqlxd.test.db.SqlDB;
import pl.edu.agh.sqlxd.test.db.QueryProvider;

public class SqlxdDB extends SqlDB {
   private static final Logger logger = Logger.getLogger(SqlxdDB.class);
   private static final String SQLXD_DRIVER = "org.sqlite.JDBC";
   
   private QueryProvider queryProvider;
   private SQLiteConnection connection;

   public SqlxdDB() {
      this(new SqlxdQueryProvider());
   }  

   public SqlxdDB(QueryProvider queryProvider) {
      super(SQLXD_DRIVER, getProperty(SQLXD_URL_KEY));
      this.queryProvider = queryProvider;
   }

   public void initConnection() throws SQLException {
      SqlDB.classForName(jdbcDriverClass);
      connection = (SQLiteConnection) DriverManager.getConnection(url);
      connection.setAutoCommit(false);
      setConnection(connection);
      logger.info("init connection");
   }

   @Override
   public void saveInvoices(List<String> filepaths) throws Exception {
      for (String filepath : filepaths) {
         connection.loadXML(filepath);
         connection.commit();
      }
   }
   
   public void createInvoicesTable() throws SQLException {
      throw new UnsupportedOperationException();
   }
   public void clearInvoicesTable() throws SQLException {
      connection.removeSimilar("invoice");
      connection.deleteAllData();
   }

   public void closeConnection() throws SQLException {
      logger.info("close connection");    
   }
   protected QueryProvider getQueryProvider() {
      return queryProvider;
   }

   @Override
   public String getDatabaseName() {
      return "SQLxD";
   }

   protected SQLiteConnection getConnection() {
      return connection;
   }
   
}
