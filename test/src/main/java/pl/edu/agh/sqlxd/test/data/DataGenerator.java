package pl.edu.agh.sqlxd.test.data;

import pl.edu.agh.sqlxd.test.SqlxdMain;
import pl.edu.agh.sqlxd.test.data.json.JsonGenerator;
import pl.edu.agh.sqlxd.test.data.xml.XmlGenerator;

import javax.xml.bind.JAXBException;

@SuppressWarnings("unused")
public class DataGenerator {

   public static void main(String[] args) throws Exception {
      final int numberOfInvoices = 500;

      generateXmls(SqlxdMain.pathXml, numberOfInvoices);
//      generateJsons(SqlxdMain.path, numberOfInvoices);
   }

   private static void generateXmls(final String outputDir, final int numberOfInvoices) {
      try {
         final XmlGenerator xmlGenerator = new XmlGenerator(outputDir);
         xmlGenerator.createInvoices(numberOfInvoices);
      } catch (JAXBException e) {
         throw new RuntimeException(e);
      }
   }

   private static void generateJsons(final String outputDir, final int numberOfInvoices) {
      JsonGenerator jsonGenerator = new JsonGenerator(outputDir);
      jsonGenerator.createInvoices(numberOfInvoices);
   }
}
