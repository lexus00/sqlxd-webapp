package pl.edu.agh.sqlxd.test.db.oracle;

import static pl.edu.agh.sqlxd.test.sql.QueryType.INSERT_INVOICE;
import static pl.edu.agh.sqlxd.test.utils.PropertiesUtil.ORA_PASSWORD_KEY;
import static pl.edu.agh.sqlxd.test.utils.PropertiesUtil.ORA_URL_KEY;
import static pl.edu.agh.sqlxd.test.utils.PropertiesUtil.ORA_USER_KEY;
import static pl.edu.agh.sqlxd.test.utils.PropertiesUtil.getProperty;

import java.io.Reader;
import java.sql.PreparedStatement;
import java.util.List;

import pl.edu.agh.sqlxd.test.db.QueryProvider;
import pl.edu.agh.sqlxd.test.db.SqlDB;
import pl.edu.agh.sqlxd.test.utils.FileUtils;

public class OracleDB extends SqlDB {
   private static final String ORA_DRIVER = "oracle.jdbc.driver.OracleDriver";
   
   private QueryProvider queryProvider;
   
   public OracleDB() {
      super(ORA_DRIVER, getProperty(ORA_URL_KEY), getProperty(ORA_USER_KEY), getProperty(ORA_PASSWORD_KEY));
      this.queryProvider = new OraQueryProvider();
   }

   public OracleDB(QueryProvider queryProvider) {
      super(ORA_DRIVER, getProperty(ORA_URL_KEY), getProperty(ORA_USER_KEY), getProperty(ORA_PASSWORD_KEY));
      this.queryProvider = queryProvider;
   }

   @Override
   public void saveInvoices(List<String> files) throws Exception {
      final PreparedStatement insertPreparedStmt = connection().prepareStatement(getQuery(INSERT_INVOICE));     
      for (String file : files) {
         final Reader reader = FileUtils.fileToReader(file);
         insertPreparedStmt.setClob(1, reader);
         insertPreparedStmt.executeUpdate();
         reader.close();
      }
      insertPreparedStmt.close();
      connection().commit();
   }

   @Override
   protected QueryProvider getQueryProvider() {
      return queryProvider;
   }
   

   @Override
   public String getDatabaseName() {
      return "Oracle";
   }
 
}
