package pl.edu.agh.sqlxd.test.utils;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PropertiesUtil {      
   private static final String PROPERTIES_PATH = "test.properties";
   
   public static final String DB2_URL_KEY = "db2.url";
   public static final String DB2_USER_KEY = "db2.user";
   public static final String DB2_PASSWORD_KEY = "db2.password";
   
   public static final String ORA_URL_KEY = "oracle.url";
   public static final String ORA_USER_KEY = "oracle.user";
   public static final String ORA_PASSWORD_KEY = "oracle.password";
   
   public static final String SQLXD_URL_KEY = "sqlxd.url";

   public static final String MONGODB_HOST_KEY = "mongodb.url";
   public static final String MONGODB_PORT_KEY = "mongodb.port";

   private static Properties properties;
   
   
   static {
      try {
         final InputStream is = FileUtils.resourceAsStream(PROPERTIES_PATH);      
         properties = new Properties();
         properties.load(is);
         is.close();
      } catch (IOException e) {
         System.err.println("Cannot read properties file: " + PROPERTIES_PATH);
         System.exit(-1);
      }
   }
   
   
   public static String getProperty(final String key) {
      return properties.getProperty(key);
   }
}
