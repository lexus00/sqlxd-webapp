package pl.edu.agh.sqlxd.test.data.invoice;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import com.fasterxml.jackson.annotation.JsonRootName;

@JsonRootName(value="invoice")
@XmlRootElement(name = "invoice")
@XmlAccessorType(XmlAccessType.FIELD)
public class Invoice {

   @XmlElement
   protected String number;

   @XmlElement(name = "EpochTime")
   @XmlJavaTypeAdapter(DateAdapter.class)
   protected Date date;

   @XmlElement
   protected Buyer buyer;

   @XmlElement(name = "product")
   protected List<Product> products;

   public Invoice() {
   }

   public Invoice(String number, Date date, Buyer buyer) {
      super();
      this.number = number;
      this.date = date;
      this.buyer = buyer;
   }

   public List<Product> getProducts() {
      if (products == null)
         products = new ArrayList<Product>();
      return products;
   }

   public String getNumber() {
      return number;
   }

   public void setNumber(String number) {
      this.number = number;
   }

   public Date getDate() {
      return date;
   }

   public void setDate(Date date) {
      this.date = date;
   }

   public Buyer getBuyer() {
      return buyer;
   }

   public void setBuyer(Buyer buyer) {
      this.buyer = buyer;
   }
}
