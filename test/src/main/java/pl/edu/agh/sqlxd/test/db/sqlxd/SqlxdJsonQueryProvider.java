package pl.edu.agh.sqlxd.test.db.sqlxd;

import static pl.edu.agh.sqlxd.test.sql.QueryType.COUNT_ROWS;
import static pl.edu.agh.sqlxd.test.sql.QueryType.GET_INVOICES_NUMBER;
import static pl.edu.agh.sqlxd.test.sql.QueryType.GET_PRODUCTS_DATA;
import static pl.edu.agh.sqlxd.test.sql.QueryType.GET_TOTAL_INCOME_AFTER_SOME_TIME;
import static pl.edu.agh.sqlxd.test.sql.QueryType.GROUP_BY_INVOICE_ID_ORDER_BY_INVOICE_VALUE;
import static pl.edu.agh.sqlxd.test.sql.QueryType.GROUP_BY_PRODUCT_ID;
import static pl.edu.agh.sqlxd.test.sql.QueryType.IS_INVOICES_TABLE_EXISTS;

import java.util.HashMap;
import java.util.Map;

import pl.edu.agh.sqlxd.test.db.QueryProvider;
import pl.edu.agh.sqlxd.test.sql.QueryType;

public class SqlxdJsonQueryProvider extends QueryProvider {

   public static final String SELECT_ALL_FILES = "select id, doc_name, loaded_date, type from sqlxd_store_table;";
   
   private Map<QueryType, String> queries;

   
   public SqlxdJsonQueryProvider() {
      initQueries();
   }

   protected Map<QueryType, String> getQueries() {
      return queries;
   }

   private void initQueries() {
      queries = new HashMap<>();

      queries.put(IS_INVOICES_TABLE_EXISTS, "SELECT 1 FROM sqlxd_store_table "
            + "WHERE upper(doc_name) = 'INVOICE'");

      
      queries.put(COUNT_ROWS, "SELECT count(*) FROM sqlxd_store_table WHERE upper(doc_name) = 'INVOICE'");
     
      queries.put(GET_INVOICES_NUMBER, "SELECTXML inv.number FROM invoice as inv");
      
      queries.put(GET_PRODUCTS_DATA, "SELECTXML inv.number as invoice_number, "
            + "  CAST(prod.id as INTEGER) as product_id, prod.code, "
            + "  CAST(prod.totalCost as REAL) as totalCost, CAST(prod.amount as INTEGER) as amount "
            + " FROM invoice as inv NATURAL JOIN inv.products as prod");      

      queries.put(GROUP_BY_PRODUCT_ID, "SELECTXML CAST(prod.id as INTEGER) as product_id, prod.code as code, "
            + "  SUM(CAST(prod.totalCost as REAL)) as sum_total_cost "           
            + " FROM invoice.products as prod");
      

      queries.put(GROUP_BY_INVOICE_ID_ORDER_BY_INVOICE_VALUE, "SELECTXML inv.number as invoice_number, inv.buyer.code as buyer_code, "
            + "  SUM(CAST(prod.totalCost as REAL)) as sum_total_cost"
            + " FROM invoice as inv NATURAL JOIN inv.products as prod"
            + " GROUP BY invoice_number, buyer_code ORDER BY sum_total_cost");
                       

      queries.put(GET_TOTAL_INCOME_AFTER_SOME_TIME, "SELECTXML COALESCE(SUM(CAST(prod.totalCost as REAL)), 0) as total_income"
            + " FROM invoice as inv NATURAL JOIN inv.products as prod"
            + " WHERE CAST(inv.date as INTEGER) >= ?");                       
   }
}
