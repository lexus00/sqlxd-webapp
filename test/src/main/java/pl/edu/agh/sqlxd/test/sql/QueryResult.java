package pl.edu.agh.sqlxd.test.sql;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class QueryResult {
   private int columnsNumber;
   private final List<String> columnsNames;
   private final List<String> columnsTypes;
   private List<List<String>> rows;

   private QueryResult() {
      this.rows = new ArrayList<>();
      this.columnsTypes = new ArrayList<>();
      this.columnsNames = new ArrayList<>();
   }

   public QueryResult(List<String> columnsNames) {
      this();
      this.columnsNames.addAll(columnsNames);
      this.columnsNumber = columnsNames.size();
   }

   public QueryResult(ResultSetMetaData metaData) throws SQLException {
      this();
      this.columnsNumber = metaData.getColumnCount();
      for (int idx=1; idx<=columnsNumber; ++idx){
         this.columnsNames.add(metaData.getColumnName(idx));
         this.columnsTypes.add(metaData.getColumnTypeName(idx));
      }
   }

   public void addRow(ResultSet rowDB) throws SQLException {
      List<String> row = new ArrayList<>();
      for (int idx=1; idx<=columnsNumber; ++idx){
         row.add(rowDB.getString(idx));
      }
      rows.add(row);
   }

   public void addRow(List<String> row) {
      rows.add(new ArrayList<>(row));
   }

   @Override
   public String toString() {
      StringBuilder sb = new StringBuilder();
      for (int idx=0; idx<columnsNumber; ++idx) {
         if (columnsTypes.size() <= idx) {
            sb.append(String.format("%s | ", columnsNames.get(idx)));
         } else {
            sb.append(String.format("%s (%s) | ", columnsNames.get(idx), columnsTypes.get(idx)));
         }
      }
      for (List<String> row : rows) {
         sb.append('\n');
         for (String value : row) {
            sb.append(String.format("%s | ", value));
         }
      }
      
      return sb.toString();
   }
   
   
}
