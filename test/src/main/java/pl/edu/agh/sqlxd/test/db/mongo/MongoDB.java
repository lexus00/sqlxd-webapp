package pl.edu.agh.sqlxd.test.db.mongo;

import com.mongodb.BasicDBObject;
import com.mongodb.MongoClient;
import com.mongodb.client.AggregateIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;
import org.bson.conversions.Bson;
import pl.edu.agh.sqlxd.test.db.DB;
import pl.edu.agh.sqlxd.test.sql.QueryResult;
import pl.edu.agh.sqlxd.test.sql.QueryType;
import pl.edu.agh.sqlxd.test.utils.FileUtils;
import pl.edu.agh.sqlxd.test.utils.PropertiesUtil;

import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.function.Consumer;
import java.util.stream.Collectors;

public class MongoDB implements DB {

   private MongoClient mongoClient;
   private MongoCollection<Document> invoices;
   private MongoDbQueryProvider mongoDbQueryProvider;

   @Override
   public String getDatabaseName() {
      return "MongoDB";
   }

   @Override
   public void initConnection() {
      final String host = PropertiesUtil.getProperty(PropertiesUtil.MONGODB_HOST_KEY);
      final String port = PropertiesUtil.getProperty(PropertiesUtil.MONGODB_PORT_KEY);
      this.mongoClient = new MongoClient(host, Integer.parseInt(port) );
      this.invoices = mongoClient.getDatabase("test").getCollection("invoices");
      this.mongoDbQueryProvider = new MongoDbQueryProvider();
   }

   @Override
   public void closeConnection() {
      mongoClient.close();
   }

   @Override
   public boolean isInvoicesTableExists() throws SQLException {
      return true;
   }

   @Override
   public void clearInvoicesTable() throws SQLException {
      invoices.deleteMany(new BasicDBObject());
   }

   @Override
   public Long countInvoicesTableRows() throws SQLException {
      return invoices.count();
   }

   @Override
   public void saveInvoices(final List<String> files) throws Exception {
      for (String file : files) {
         final String json = FileUtils.readFileToString(file);
         final Document doc = Document.parse(json);
         invoices.insertOne(doc);
      }
   }

   @Override
   public QueryResult query(final QueryType queryType) throws SQLException {
      final List<String> columnsName = mongoDbQueryProvider.getColumnsName(queryType);
      final QueryResult result = new QueryResult(columnsName);

      if (queryType == QueryType.COUNT_ROWS) {
         final Long count = countInvoicesTableRows();
         result.addRow(Arrays.asList(count.toString()));
      } else {
         final List<? extends Bson> aggregatePipeline = mongoDbQueryProvider.getAggregatePipeline(queryType);
         final AggregateIterable<Document> aggregate = invoices.aggregate(aggregatePipeline);
         aggregate.forEach((Consumer<? super Document>) document -> appendDocument(columnsName, result, document));
      }

      return result;
   }

   private void appendDocument(final List<String> columnsName, final QueryResult result, final Document document) {
      final List<String> rowValues = columnsName.stream().map(col -> Objects.toString(document.get(col))).collect(Collectors.toList());
      result.addRow(rowValues);
   }

}
