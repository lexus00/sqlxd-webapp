<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
	<title>SQLxD</title>
	<link rel="shortcut icon" href="${pageContext.request.contextPath}/resources/favicon.ico" type="image/x-ico; charset=binary"/>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"></meta>
	
	
	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/sqlxd.css" type="text/css" media="all" />	
	<link rel="stylesheet" 
		href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
	
  	<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
	<script src="http://malsup.github.com/jquery.form.js"></script>
	
	<script type="text/javascript">
	var ctxPath = "${pageContext.request.contextPath}";
	</script>
	<script src="${pageContext.request.contextPath}/resources/data.js"></script>
	
</head>

<body style="margin:0px;">
	<div class="outer">
		<div class="middle">
			<div class="inner" style="width: 900px; text-align: center;">
			
				<div style="width: 400px; margin: auto;" >  <!-- LOADED FILES -->
					<table width="400px" class="border"> 
						<colgroup>
							<col width="50px">
							<col >
							<col width="150px">
							<col width="70px">
						</colgroup>
						<thead class="table-bordered">
							<tr>
								<th colspan="4" align="center" style="padding-bottom: 10px;">
									<span>
										<img alt="Logo AGH" 
											src="${pageContext.request.contextPath}/resources/iet.jpg" 
											width="25px"/>										
										<label style="font-size: 25px;">SQLxD</label>
									</span>	
								</th>
							</tr>
							<tr>
								<th colspan="4" align="center" style="padding-bottom: 10px;">
									<label>FILES</label>
									
									<div style="display: inline;margin-left: 40px; ">
										<input name="uploadfile" id="uploadfile" type="file"/>
										<label style="font-weight: 100; cursor: pointer;" 
												onclick="proxyUploadClick()">
											Add
										</label>
									</div>
								</th>
							</tr>
							<tr>
								<th><label>Id</label></th>
								<th><label>Root name</label></th>
								<th><label>Loaded time</label></th>
								<th></th>
							</tr>
						</thead>
						<tbody>
							<c:forEach var="file" items="${vm.files}">
								<tr>
									<td align="center"><label>${file.id}</label></td>
									<td><label style="margin-left: 5px;">${file.root}</label></td>
									<td align="center">
										<fmt:formatDate value="${file.loaded}" 
											pattern="yyyy.MM.dd HH:mm:ss" />
									</td>
									<td align="center">
										<span style="cursor: pointer;">														
											<a href="${pageContext.request.contextPath}/delete/${file.id}" 
													style="color: black; text-decoration: none;">
												<i class="fa fa-trash-o" aria-hidden="true"></i>
											</a>
											<a href="${pageContext.request.contextPath}/download/${file.id}" 
													style="color: black; margin-left: 10px;text-decoration: none;">
												<i class="fa fa-download" aria-hidden="true"></i>
											</a>
										</span>										
									</td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
				
				
				<form:form commandName="vm" action="${pageContext.request.contextPath}/data/" method="POST">
					<div style="margin:20px 0px 5px;">
						<form:textarea path="query"
							placeholder="Insert query here" 
							rows="3" cols="60"/>
					</div>		
					<div style="margin:0px 0px 15px;">
						<input type="submit" name="submit" value="Execute"/>
					</div>							
				</form:form>
				
				
				<c:if test="${not empty vm.error}">
					<p>Error: <label>${vm.error}</label></p>					
				</c:if>
				
				<c:if test="${not empty vm.table}">
					<div style="margin:auto;width:800px;height:400px;overflow:auto;">					
						<table class="border" width="800px" cellpadding="0" cellspacing="0" border="0">
							<colgroup>
								<c:forEach var="col" items="${vm.table.columns}">					
									<col>
								</c:forEach>
							</colgroup>
							<thead>
								<tr>
									<c:forEach var="col" items="${vm.table.columns}">					
										<th><label style="white-space: nowrap;margin: 0px 2px 0px 2px;">${col}</label></th>
									</c:forEach>
								</tr>
							</thead>
							<tbody>
								<c:forEach var="row" items="${vm.table.rows}">
									<tr>
										<c:forEach var="cell" items="${row.values}">					
											<td><label>${cell}</label></td>
										</c:forEach>
									</tr>
								</c:forEach>
							</tbody>
						</table>									
					</div>
				</c:if>
				
			</div>			
		</div>
	</div>
	
</body>
</html>