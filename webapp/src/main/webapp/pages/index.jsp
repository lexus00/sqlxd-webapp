<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<html>
<head>
	<title>SQLxD</title>
	<link rel="shortcut icon" href="/sqlxd/resources/favicon.ico" type="image/x-ico; charset=binary"/>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"></meta>
	<meta charset="UTF-8"></meta>
	
	<link rel="stylesheet" href="/sqlxd/resources/sqlxd.css" type="text/css" media="all" />
</head>

<body>
	<div class="outer">
		<div class="middle">
			<div class="inner" style="width: 200px">
				<form:form action="submit" commandName="credential" method="GET">
					<table>
						<colgroup>
							<col width="50%">
							<col width="50%">
						</colgroup>
						<tbody>
							<tr>
								<td colspan="2" align="center">
									<span>
										<img alt="Logo AGH" 
											src="resources/iet.jpg" 
											width="25px"/>										
										<label style="font-size: 25px;">SQLxD</label>
									</span>																											
								</td>
							</tr>
							<tr style="height: 10px"/>
							<tr>
								<td>Login</td>
								<td><form:input path="login"/></td>
							</tr>
							<tr style="height: 10px"/>
							<tr>
								<td colspan="2" align="center">
									<input type="submit" value="Submit"/>
								</td>
							</tr>
							<tr style="height: 100px"/>
						</tbody>
					</table>
				</form:form>
			</div>			
		</div>
	</div>
</body>

</html>