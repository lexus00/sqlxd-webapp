//using jquery.form.js
function uploadJqueryForm() {
	$('#result').html('');

	$("#form2").ajaxForm({
		success : function(data) {
			$('#result').html(data);
		},
		dataType : "text"
	}).submit();
}

function proxyUploadClick(){
	$("#uploadfile").click();
}

$(document).on("change", "#uploadfile", function() {		
	var formData = new FormData();	
	formData.append("file", uploadfile.files[0])	

	$.ajax({
		url : ctxPath + "/upload",
		type : "POST",
		dataType : 'text',
		contentType : false,
		processData : false,
		cache : false,
		data : formData,
		success : function(response) {/*alert("success");*/
			console.log(response);
			location.reload();
		},
		error : function() {/*alert("unable to create the record");	*/}
	});		

});

$("#uploadbutton").on("click", function() {
	var formData = new FormData($('#uploadForm')[0]);
	$.ajax({
		url : ctxPath + "/upload",
		type : "POST",
		dataType : 'text',
		contentType : false,
		processData : false,
		cache : false,
		data : formData,
		success : function(response) {
			alert("success");
		},
		error : function() {
			alert("unable to create the record");
		}
	});
})


$(document).on("click", "#upload", function() {
	var file_data = $("#avatar").prop("files")[0];   // Getting the properties of file from file field
	var form_data = new FormData();                  // Creating object of FormData class
	form_data.append("file", file_data)              // Appending parameter named file with properties of file_field to form_data
	form_data.append("user_id", 123)                 // Adding extra parameters to form_data
	$.ajax({
                url: ctxPath + "/upload",
                dataType: 'script',
                cache: false,
                contentType: false,
                processData: false,
                data: form_data,                         // Setting the data attribute of ajax with file_data
                type: 'post'
       })
})