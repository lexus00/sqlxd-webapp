package pl.edu.agh.sqlxd.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Table implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private Row columns, columnsTypes;
	private List<Row> rows;
	
	public Table(){
		this.columns = new Row();
		this.columnsTypes = new Row();
		this.rows = new ArrayList<Row>();
	}

	public List<String> getColumns(){
		return columns.getValues();
	}
	public Row getHeaderRow() {
		return columns;
	}
	public List<Row> getRows() {
		return rows;
	}
	public Row getColumnsTypes() {
		return columnsTypes;
	}
	public Row nextRow(){
		Row row = new Row();
		rows.add(row);
		return row;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(columns);
		for (Row row : rows)
			sb.append("\n" + row);
		return sb.toString();
	}
	
}
