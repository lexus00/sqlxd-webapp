package pl.edu.agh.sqlxd.controller;

import java.sql.Date;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import javax.activation.UnsupportedDataTypeException;

import org.apache.log4j.Logger;
import org.sqlite.SQLiteConnection;

import pl.edu.agh.sqlxd.model.File;
import pl.edu.agh.sqlxd.model.FileType;
import pl.edu.agh.sqlxd.model.Row;
import pl.edu.agh.sqlxd.model.Table;



public class SqlxdDAO {
	private static final Logger logger = Logger.getLogger(SqlxdDAO.class);

	private static final Pattern DB_NAME_PATTERN = Pattern.compile("^[a-zA-Z0-9]{3,20}\\.db$");
	private static final String DB_NAME_ERR_MSQ = "database's name must match pattern:  ^[a-zA-Z0-9]{3,20}\\.db$";
	private static final String SELECT_ALL_FILES = "select id, doc_name, loaded_date, type from sqlxd_store_table;";
	private static final String SELECT_FILE = "select id, doc_name, type, doc FROM sqlxd_store_table where id=%d;";

	private final String dbname;
	private final SQLiteConnection conn;

	static {
		try {
			Class.forName("org.sqlite.JDBC");
		} catch (ClassNotFoundException e) {
			logger.error(e.getMessage(), e);
			throw new ExceptionInInitializerError(e);
		}
	}

	/**
	 * @see #verifyDBName(String)
	 */
	public SqlxdDAO(String dbname) throws SQLException {
		verifyDBName(dbname);
		this.dbname = dbname;
		this.conn = (SQLiteConnection) DriverManager.getConnection("jdbc:sqlite:" + dbname);
	}
	
	/**
	 * <b>if name is not correct, the exception will be thrown</b>
	 * <p>database name must match pattern ^[a-zA-Z0-9]{3,20}\.db$  </p>
	 */
	public static void verifyDBName(String dbname) throws SQLException {
		if ( !":memory:".equals(dbname) && !DB_NAME_PATTERN.matcher(dbname).matches() )
			throw new SQLException(DB_NAME_ERR_MSQ);		
	}

	public List<File> getFiles() {
		List<File> files = new ArrayList<File>();
		try {
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(SELECT_ALL_FILES);
	
			while (rs.next()) {
				File file = new File();
				file.setId(rs.getLong(1));
				file.setRoot(rs.getString(2));
				file.setLoaded(new Date(rs.getLong(3) * 1000));
				file.setType(FileType.fromInt(rs.getInt(4)));
				files.add(file);
			}
			rs.close();
			stmt.close();
		} catch (SQLException ex){
			logger.error(ex.getMessage(), ex);
		}
		return files;
	}
	
	public void uploadFile(String filepath) throws UnsupportedDataTypeException, SQLException {
		logger.debug(filepath);
		if (filepath.endsWith(".xml"))
			conn.loadXML(filepath);
		else if (filepath.endsWith(".json"))
			conn.loadJSON(filepath);
		else
			throw new UnsupportedDataTypeException("sqlxd supports only xml and json files");

	}
	
	public File getFile(long fileId) throws SQLException{
		// select id, xmlDocName, type, xml
		String sql = String.format(SELECT_FILE, fileId);
		logger.debug("q = " + sql);
		Statement stmt = conn.createStatement();
		ResultSet rs = stmt.executeQuery(sql);

		File file = null;
		if (rs.next()){
			file = new File();
			file.setId(rs.getLong(1));
			file.setRoot(rs.getString(2));
			file.setType( FileType.fromInt(rs.getInt(3)) );
			file.setData(rs.getString(4));
		}
		
		rs.close();
		stmt.close();
		return file;
	}

	public void delete(long fileId) throws SQLException {
		logger.debug( "file id: " + fileId);
		conn.deleteFile(fileId);
	}

	public Table execute(String query) throws SQLException {
		logger.debug(query);
		Statement stmt = conn.createStatement();
		ResultSet rs = stmt.executeQuery(query);
		Table table = new Table();
		
		ResultSetMetaData meta = rs.getMetaData();
		for (int i = 1; i <= meta.getColumnCount(); ++i) {
			table.getHeaderRow().add(meta.getColumnLabel(i));
			table.getColumnsTypes().add(meta.getColumnTypeName(i));
		}
	
		while (rs.next()) {
			Row row = table.nextRow();
			for (int i = 1; i <= meta.getColumnCount(); ++i)
				row.add(rs.getString(i));
		}

		rs.close();
		stmt.close();
		logger.debug("number of rows: " + table.getRows().size());
		return table;
	}

	public SQLiteConnection getConn() {
		return conn;
	}

	public String getDbname() {
		return dbname;
	}
}
