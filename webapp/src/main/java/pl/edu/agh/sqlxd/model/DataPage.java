package pl.edu.agh.sqlxd.model;

import java.util.List;

public class DataPage {
	
	private Credential credential;
	private List<File> files;
	private Table table;
	private String query;// = "selectxml p.* from people.person as p;";
	
	private String error;
	
	public DataPage(){
	}
	public DataPage(Credential credential, List<File> files) {
		this.credential = credential;
		this.files = files;
	}
	public DataPage(Credential credential, String error, List<File> files, Table table) {
		this.credential = credential;
		this.files = files;
		this.table = table;
		this.error = error;
	}
	

	@Override
	public String toString() {
		return "DataPage " + System.identityHashCode(this) + ", er: " + error;
	}	
	
	
	public Credential getCredential() {
		return credential;
	}
	public void setCredential(Credential credential) {
		this.credential = credential;
	}
	public List<File> getFiles() {
		return files;
	}
	public void setFiles(List<File> files) {
		this.files = files;
	}
	public Table getTable() {
		return table;
	}
	public void setTable(Table table) {
		this.table = table;
	}
	public String getError() {
		return error;
	}
	public void setError(String error) {
		this.error = error;
	}
	public String getQuery() {
		return query;
	}
	public void setQuery(String query) {
		this.query = query;
	}
}
