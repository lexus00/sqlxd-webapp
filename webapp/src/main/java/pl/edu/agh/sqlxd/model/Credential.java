package pl.edu.agh.sqlxd.model;

public class Credential {
	private String login;


	public Credential() {
	}
	public Credential(String login) {
		this.login = login;
	}
	
	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	
	@Override
	public String toString() {
		return "Credential [login=" + login + "]";
	}
}
