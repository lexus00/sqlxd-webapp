package pl.edu.agh.sqlxd.controller;

import java.util.Map.Entry;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import pl.edu.agh.sqlxd.model.Credential;

@Controller
public class LoginContr {
	private static final Logger logger = Logger.getLogger(LoginContr.class);
	private static final String VIEW_INDEX = "index";
	
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String init(ModelMap model, HttpServletRequest request) {
//		logger.debug("GET");
//		model.addAttribute("credential", new Credential());
//		return VIEW_INDEX;
		
		request.getSession().setAttribute("credential", new Credential("user"));
		
		return "redirect:/data/";

	}
	
	@RequestMapping(name="/", method=RequestMethod.POST)
	public String login(@ModelAttribute("credential")Credential credential, ModelMap model){
		logger.debug("POST");
		
		Object login = model.get("credential");
		System.out.println(login);
		System.err.println(credential);
		
		for (Entry<String, Object> entry : model.entrySet())
			System.out.println(entry.getValue() + " = " + entry.getValue());
		
		return VIEW_INDEX;
	}
	
	@RequestMapping(value = "/submit", method = RequestMethod.GET)
	public String login(@RequestParam String login, HttpServletRequest request) {
		logger.debug("GET");
		System.out.println(login);
			
		request.getSession().setAttribute("credential", new Credential(login));
				
		return "redirect:/data/";
	}
}
