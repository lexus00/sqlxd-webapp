package pl.edu.agh.sqlxd.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Row implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private ArrayList<String> values;
	
	
	public Row(){
		values = new ArrayList<String>();
	}
	
	
	public void add(String v){
		values.add(v == null ? "" : v);
	}
		
	public void get(int idx){
		values.get(idx);
	}
	
	public List<String> getValues() {
		return new ArrayList<String>(values);
	}

	public int size() {
		return values.size();
	}

	@Override
	public String toString() {
		return values.toString() ;
	}
	
}
