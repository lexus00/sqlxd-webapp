package pl.edu.agh.sqlxd;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import javax.activation.UnsupportedDataTypeException;

import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import org.sqlite.SQLiteConnection;

import pl.edu.agh.sqlxd.controller.SqlxdDAO;
import pl.edu.agh.sqlxd.model.Table;


@SuppressWarnings("unused")
public class SQlxDApp {
	private static final Logger logger = Logger.getLogger(SQlxDApp.class);
	private static String PEOPLE_XML = "C:/Users/Blazej/workspace-agh/sqlxd/"
			+ "src/main/resources/people.xml";
	
	public static void main(String[] args) throws Exception {
		SqlxdDAO dao = new SqlxdDAO("database.db");
//		dao.uploadFile(PEOPLE_XML);
		List<pl.edu.agh.sqlxd.model.File> list = dao.getFiles();
		
		for (Object f : list)
			System.out.println(f);
		
//		pl.edu.agh.sqlxd.model.File is = dao.getFile(1);
//		System.out.println(is);
//		if (is != null)
//			System.out.println(is.getData());
		
//		temp1();
		temp2();
	}



	private static void temp2() throws SQLException, UnsupportedDataTypeException {
		SqlxdDAO dao = new SqlxdDAO(":memory:");
		dao.uploadFile(PEOPLE_XML);
		Table table = dao.execute("selectxml person.* from people.person as person");
		System.out.println(table);
		
		table = dao.execute("pragma table_info(sqlxd_store_table)");
		System.out.println(table);
		
//		dao.getFiles();		
//		temp1();  native libs dir
	}
	
	private static void temp1() throws ClassNotFoundException, SQLException {
		Class.forName("org.sqlite.JDBC");
        SQLiteConnection conn = (SQLiteConnection) DriverManager
        		.getConnection("jdbc:sqlite:database.db");
        conn.deleteAllData();
        conn.loadXML(PEOPLE_XML);
        
        Statement stmt = conn.createStatement();
        ResultSet rs = stmt.executeQuery("selectxml * from tableXML;");
        
//        Statement stmt = conn.createStatement();
//        stmt.executeUpdate("create table sample(id, name)");
//        stmt.executeUpdate("insert into sample values(1, \"leo\")");
//        stmt.executeUpdate("insert into sample values(2, \"yui\")");
//
//        stmt.executeUpdate("backup to " + tmpFile.getAbsolutePath());
//        stmt.close();

        // open another memory database
//        Connection conn2 = DriverManager.getConnection("jdbc:sqlite:");
//        Statement stmt2 = conn.createStatement();
//        ResultSet rs = stmt2.executeQuery("selectxml person.* from people.person as person;");
//        
////        ResultSetMetaData meta = rs.getMetaData();
////        for (int i=1; i<=meta.getColumnCount(); ++i)
////        	System.out.println(meta.getColumnLabel(i) + " " + meta.getColumnTypeName(i));
//        
//        
//        int count = 0;
//        while (rs.next()) {
//        	
//            count++;
//        }
//        
//        System.out.println(count);
////        assertEquals(2, count);
//        rs.close();
	}
	

}
