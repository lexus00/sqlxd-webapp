package pl.edu.agh.sqlxd.model;

import java.text.SimpleDateFormat;
import java.util.Date;

public class File {
	
	private long id;
	private String root;
	private Date loaded;
	private String data;
	private FileType type;
	
	private SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd HH:mm");
	
	public File(){
	}
	public File(long id, String root, Date loaded) {
		this.id = id;
		this.root = root;
		this.loaded = loaded;
	}

	
	@Override
	public String toString() {
		return "File [id=" + id + ", root=" + root + ", loaded=" 
				+ getLoadedStr() + ", type=" + type 
				+ "data is null=" + (data == null) + "]";
	}
	
	
	public String getLoadedStr(){
		return loaded == null ? null : sdf.format(loaded);
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getRoot() {
		return root;
	}
	public void setRoot(String root) {
		this.root = root;
	}
	public Date getLoaded() {
		return loaded;
	}
	public void setLoaded(Date loaded) {
		this.loaded = loaded;
	}
	public FileType getType() {
		return type;
	}
	public void setType(FileType type) {
		this.type = type;
	}
	public String getData() {
		return data;
	}
	public void setData(String data) {
		this.data = data;
	}	
}
