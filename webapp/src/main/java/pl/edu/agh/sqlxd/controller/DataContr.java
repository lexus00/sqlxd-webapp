package pl.edu.agh.sqlxd.controller;

import static java.io.File.createTempFile;

import java.io.FileOutputStream;
import java.io.OutputStream;
import java.sql.SQLException;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import pl.edu.agh.sqlxd.model.Credential;
import pl.edu.agh.sqlxd.model.DataPage;
import pl.edu.agh.sqlxd.model.File;
import pl.edu.agh.sqlxd.model.FileType;
import pl.edu.agh.sqlxd.model.Table;

@Controller
public class DataContr {
	private static final Logger logger = Logger.getLogger(LoginContr.class);
		
	private SqlxdDAO dao;
	
		
	@RequestMapping(value = "/data/", method = RequestMethod.GET)
	public String init(HttpServletRequest request, ModelMap model) throws SQLException {
		Credential credential = (Credential)request.getSession().getAttribute("credential");
		logger.debug("GET " + credential);
		if (credential == null)
			return "redirect:/";	
		
		model.addAttribute("vm", new DataPage(credential, getDao().getFiles()));
		return "data";
	}
	
	@RequestMapping(value="/data/", method=RequestMethod.POST)
	public String query(@ModelAttribute("vm") DataPage dp, ModelMap model){
		DataPage dataPage = (DataPage) model.get("vm");
		dataPage.setFiles(getDao().getFiles());
		logger.debug("query: " + dataPage.getQuery());
		try {
			Table table = getDao().execute( dataPage.getQuery() );
			dataPage.setTable(table);
		} catch (SQLException e) {
			logger.warn(e.getMessage());
			dataPage.setError(e.getMessage());
		}
		
		model.addAttribute("vm", dataPage);
		return "data";
	}
	
	
	@RequestMapping(value = "/upload", method = RequestMethod.POST)
	public String upload(@ModelAttribute("vm") DataPage dp, ModelMap model, 
			MultipartHttpServletRequest request ) {
		logger.debug("POST");	
		DataPage dataPage = (DataPage) model.get("vm");
		for (Entry<String, MultipartFile> entry : request.getFileMap().entrySet()){								
			try {
				MultipartFile mf = entry.getValue();
				FileType fileType = getFileType(mf);
				java.io.File file = createTempFile("file", fileType.getExtension());
				FileOutputStream fos = new FileOutputStream(file);
				IOUtils.copy(mf.getInputStream(), fos);
				fos.close();
				mf.getInputStream().close();			
				getDao().uploadFile(file.getAbsolutePath());
				file.delete();
			} catch (Exception e) {
				dataPage.setError( e.getMessage() );
				logger.warn(e.getMessage());
//				logger.warn(e.getMessage(), e);
			}
		}
		
		dataPage.setFiles(getDao().getFiles());
		model.addAttribute("vm", dataPage);
		return "data";
	}
	
	@RequestMapping(value="/delete/{id}", method=RequestMethod.GET)
	public String delete(@PathVariable Long id, ModelMap model) {	
		logger.debug("Delete file id: " + id);

		DataPage dataPage = createDataPage();
		try {
			getDao().delete(id);
			dataPage.setFiles(getDao().getFiles());
		} catch (SQLException e) {
			logger.warn(e.getMessage());
			dataPage.setError(e.getMessage());
		}
		
		model.addAttribute("vm", dataPage);	
		return "data";
	}
	
	@RequestMapping(value = "/download/{id}", method = RequestMethod.GET)
	public void download(@PathVariable Long id, HttpServletResponse response) {
		logger.debug("Download file id: " + id);	
		try {
			File file = getDao().getFile(id);
			byte[] bytes = file.getData().getBytes("UTF8");
			
			response.setContentType(file.getType().getMimeType());
			response.setContentLength(bytes.length);
			
			String filename = file.getRoot() + file.getType().getExtension();
			String headerKey = "Content-Disposition";
			String headerValue = String.format("attachment; filename=\"%s\"", filename);
			response.setHeader(headerKey, headerValue);
			
			OutputStream os = response.getOutputStream();
			os.write(bytes);
			os.close();
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
	}

	
	
	// getters and setters
	private SqlxdDAO getDao() {
		if (dao == null)
			try {
				dao = new SqlxdDAO("database.db");
			} catch (SQLException e) {
				e.printStackTrace();
			}
		return dao;
	}
	private DataPage createDataPage(){
		logger.info("called");
		ServletRequestAttributes attr = 
			(ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
		HttpSession session = attr.getRequest().getSession();
		
		Credential credential = (Credential) session.getAttribute("credential");
		return new DataPage(credential, getDao().getFiles());
	}
	private FileType getFileType(MultipartFile mf) throws Exception{
		for (FileType type : FileType.values()){
			String flnm = mf.getOriginalFilename();
			String ct = mf.getContentType();
			if (flnm != null && flnm.endsWith(type.getExtension())
					|| (ct != null && type.getMimeTypes().contains(ct)))
				return type;			
		}
		
		throw new Exception("cannot load file " + mf.getOriginalFilename() 
				+ ", reason: filetype not supported [" + mf.getContentType() +"]");
	}
}
