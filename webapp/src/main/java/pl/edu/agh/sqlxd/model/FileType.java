package pl.edu.agh.sqlxd.model;

import java.util.Arrays;
import java.util.List;

public enum FileType { 
	
	XML (".xml", "application/xml", "text/xml"), 
	JSON (".json", "application/json"); 
	
	
	private final String[] mimeTypes;
	private final String extension;

	FileType(String ext, String ...mimeTypes){
		this.mimeTypes = mimeTypes;
		this.extension = ext;
	}
	
	public static FileType fromInt(int num){
		if (num == 1)
			return XML;
		if (num == 2)
			return JSON;
		throw new IllegalArgumentException("value=" + num);
	}

	public String getMimeType() {
		return mimeTypes[0];
	}
	public List<String> getMimeTypes() {
		return Arrays.asList(mimeTypes);
	}
	public String getExtension() {
		return extension;
	}

}