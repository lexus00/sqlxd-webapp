package pl.edu.agh.sqlxd.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import pl.edu.agh.sqlxd.model.File;

public class DAO {
	
	
	public List<File> getFiles(String login){
		return new ArrayList<File>(Arrays.asList(
			new File(1, "people", new Date()),
			new File(2, "people", new Date()),
			new File(3, "people", new Date()),
			new File(4, "people", new Date()),
			new File(5, "people", new Date()),
			new File(6, "PayTable", new Date()),
			new File(7, "PayTable", new Date())	
		));
	}
}
